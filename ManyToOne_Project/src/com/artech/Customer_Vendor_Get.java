package com.artech;

import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Customer_Vendor_Get {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		//VendorDto vd = (VendorDto) s.get(VendorDto.class, 1);
		//String str = vd.getProductName();
		
		CustomerDto cd = (CustomerDto) s.get(CustomerDto.class, 3);
		VendorDto vd = cd.getVendorObj();
		
		System.out.println(vd.getV_id()+"  "+vd.getV_name()+"  "+vd.getProductName());
		System.out.println(cd.getName()+"  "+cd.getAddress()+"  "+cd.getC_id() );
		s.close();
		
		
		

	}

}
