package com.artech;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Bank_Customer_Update {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		Bank_Customer_Class bcc = (Bank_Customer_Class) s.get(Bank_Customer_Class.class, 4);
		Bank_Class bc = bcc.getBankclass();
		bcc.setC_name("Bhupesh paliwal");
		bcc.setAddress("Rustam Ka Bagicha");
		bcc.setMobile_no("9945789632");
		bc.setB_address("Geeta Bhawan");
		s.update(bcc);
		t.commit();
		s.close();

	}

}
