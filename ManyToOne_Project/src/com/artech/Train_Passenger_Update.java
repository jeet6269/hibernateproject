package com.artech;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Train_Passenger_Update {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		Passengers_Class pc = (Passengers_Class) s.get(Passengers_Class.class, 578);
		Train_Class tc =pc.getTclass();
      pc.setName("Suman");
      pc.setEmail("Suman@gmail.com");
      tc.setArrival_time("12 noon");
      tc.setDepature_time("12:45");
      s.update(pc);
      t.commit();
      s.close();
      
	}

}
