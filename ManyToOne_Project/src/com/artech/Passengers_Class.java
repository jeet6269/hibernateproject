package com.artech;

public class Passengers_Class {
private  int ticket_no;
private String name,email,address;
private int mobile_no;
private Train_Class tclass;
public int getTicket_no() {
	return ticket_no;
}
public void setTicket_no(int ticket_no) {
	this.ticket_no = ticket_no;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public int getMobile_no() {
	return mobile_no;
}
public void setMobile_no(int mobile_no) {
	this.mobile_no = mobile_no;
}
public Train_Class getTclass() {
	return tclass;
}
public void setTclass(Train_Class tclass) {
	this.tclass = tclass;
}

}
