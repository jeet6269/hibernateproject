package com.artech;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Train_Passenger_Delete {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		Passengers_Class pc = (Passengers_Class) s.get(Passengers_Class.class, 7851);
		Train_Class tc =pc.getTclass();
		s.delete(pc);
		t.commit();
		s.close();

	}

}
