package com.artech;

public class Bank_Customer_Class {
 private int c_id;
 private String c_name,address,mobile_no;
 private Bank_Class bankclass;
public int getC_id() {
	return c_id;
}
public void setC_id(int c_id) {
	this.c_id = c_id;
}
public String getC_name() {
	return c_name;
}
public void setC_name(String c_name) {
	this.c_name = c_name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getMobile_no() {
	return mobile_no;
}
public void setMobile_no(String mobile_no) {
	this.mobile_no = mobile_no;
}
public Bank_Class getBankclass() {
	return bankclass;
}
public void setBankclass(Bank_Class bankclass) {
	this.bankclass = bankclass;
}
 
 
}
