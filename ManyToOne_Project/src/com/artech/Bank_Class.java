package com.artech;

public class Bank_Class {
private int b_id;
private String b_name,b_address;
public int getB_id() {
	return b_id;
}
public void setB_id(int b_id) {
	this.b_id = b_id;
}
public String getB_name() {
	return b_name;
}
public void setB_name(String b_name) {
	this.b_name = b_name;
}
public String getB_address() {
	return b_address;
}
public void setB_address(String b_address) {
	this.b_address = b_address;
}

}
