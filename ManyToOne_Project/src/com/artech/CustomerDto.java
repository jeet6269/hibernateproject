package com.artech;

public class CustomerDto {
private int c_id;
private String name,address;
private VendorDto vendorObj;
public int getC_id() {
	return c_id;
}
public void setC_id(int c_id) {
	this.c_id = c_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public VendorDto getVendorObj() {
	return vendorObj;
}
public void setVendorObj(VendorDto vendorObj) {
	this.vendorObj = vendorObj;
}


}
