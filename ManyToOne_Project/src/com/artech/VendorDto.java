package com.artech;

public class VendorDto {
private int v_id;
private String v_name,productName;
public int getV_id() {
	return v_id;
}
public void setV_id(int v_id) {
	this.v_id = v_id;
}
public String getV_name() {
	return v_name;
}
public void setV_name(String v_name) {
	this.v_name = v_name;
}
public String getProductName() {
	return productName;
}
public void setProductName(String productName) {
	this.productName = productName;
}

}
