package com.artech;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Bank_Customer_Class_Delete {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
	   Bank_Customer_Class bcc = new Bank_Customer_Class();
	   bcc.setC_id(4);
	   s.delete(bcc);
	   t.commit();
	   s.close();

	}

}
