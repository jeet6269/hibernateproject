package com.artech;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Bank_Customer_Save {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
	    Session s = sf.openSession();
	    Transaction t = s.beginTransaction();
	    Bank_Class bc = new Bank_Class();
	    bc.setB_name("panjab Bank");
	    bc.setB_address("LIG main Road");
	    Bank_Customer_Class bcc = new Bank_Customer_Class();
	    bcc.setC_name("jitendra sharma");
	    bcc.setAddress("Malviya Nagar");
	    bcc.setMobile_no("8982619527");
	    bcc.setBankclass(bc);
	    Bank_Customer_Class bcc1 = new Bank_Customer_Class();
	    bcc1.setC_name("Shyam sharma");
	    bcc1.setAddress("Nehru Nagar");
	    bcc1.setMobile_no("8103838076");
	    bcc1.setBankclass(bc);
	   
	    
	    Bank_Class bc1 = new Bank_Class();
	    bc1.setB_name("Bank of India");
	    bc1.setB_address("LIG main Road");
	    Bank_Customer_Class bcc2 = new Bank_Customer_Class();
	    bcc2.setC_name("Shubham Mishra");
	    bcc2.setAddress("MR9 Road");
	    bcc2.setMobile_no("8871126930");
	    bcc2.setBankclass(bc1);
	    Bank_Customer_Class bcc3 = new Bank_Customer_Class();
	    bcc3.setC_name("rajat sharma");
	    bcc3.setAddress("Mahesh Baag");
	    bcc3.setMobile_no("8145789620");
	    bcc3.setBankclass(bc1);
	    s.save(bcc);
	    s.save(bcc1);
	    s.save(bcc2);
	    s.save(bcc3);
	    t.commit();
	    s.close();

	}

}
