package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Train_Passenger_Save {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		Train_Class tc = new Train_Class();
		tc.setT_name("Sawarmati Express");
		tc.setArrival_time("3:00 pm");
		tc.setDepature_time("3:45 pm");
		Passengers_Class pc = new Passengers_Class();
		pc.setTicket_no(578);
		pc.setName("Ashish more");
		pc.setEmail("Ashish12@gmail.com");
        pc.setAddress("rajendra nagar");
        pc.setMobile_no(14582);
        pc.setTclass(tc);
        Passengers_Class pc1 = new Passengers_Class();
		pc1.setTicket_no(751);
		pc1.setName("Ankit more");
		pc1.setEmail("Ankit2@gmail.com");
        pc1.setAddress("rajendra nagar");
        pc1.setMobile_no(414582);
        pc1.setTclass(tc);
        Train_Class tc1 = new Train_Class();
		tc1.setT_name("ranthmbor Express");
		tc1.setArrival_time("6:00 Am");
		tc1.setDepature_time("6:45 Am");
		Passengers_Class pc2 = new Passengers_Class();
		pc2.setTicket_no(178);
		pc2.setName("pragti mishra");
		pc2.setEmail("pragti@yahoo.com");
        pc2.setAddress("malivya nagar");
        pc2.setMobile_no(1452542);
        pc2.setTclass(tc1);
        s.save(pc);
        s.save(pc1);
        s.save(pc2);
        t.commit();
        s.close();
	}

}
