package com.artech;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Customer_vendor_Delete {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		CustomerDto cd = new CustomerDto();
		cd.setC_id(4);
		s.delete(cd);
		t.commit();
		s.close();

	}

}
