package com.artech;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Bank_Customer_Class_delete1 {
	public static void main(String[] args) {
		
	
	SessionFactory sf = new Configuration().configure().buildSessionFactory();
	Session s = sf.openSession();
	Transaction t = s.beginTransaction();
	Bank_Customer_Class bcc = (Bank_Customer_Class) s.get(Bank_Customer_Class.class, 1);
	
	s.delete(bcc);
	t.commit();
	s.close();
	}
}
