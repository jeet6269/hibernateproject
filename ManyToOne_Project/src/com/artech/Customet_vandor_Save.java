package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Customet_vandor_Save {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		VendorDto vd = new VendorDto();
		vd.setV_name("Kishor Kumar");
		vd.setProductName("Dettol");
		CustomerDto cd = new CustomerDto();
		cd.setName("Shubhodh Shah");
		cd.setAddress("patniPura Squre");
		cd.setVendorObj(vd);
		CustomerDto cd2 = new CustomerDto();
		cd2.setName("rameShwar");
		cd2.setAddress("Nanda nagar");
		cd2.setVendorObj(vd);
		VendorDto vd1 = new VendorDto();
		vd1.setV_name("Bhupesh Paliwal");
		vd1.setProductName("garnier Shempoo");
		CustomerDto cd3 = new CustomerDto();
		cd3.setName("Ramesh");
		cd3.setAddress("Malwa mil");
		cd3.setVendorObj(vd1);
		CustomerDto cd4 = new CustomerDto();
		cd4.setName("Suresh");
		cd4.setAddress("Shree Nagar");
		cd4.setVendorObj(vd1);
		CustomerDto cd5 = new CustomerDto();
		cd5.setName("Ashoke");
		cd5.setAddress("Nehru nagar");
		cd5.setVendorObj(vd1);
		s.save(cd);
		s.save(cd2);
		s.save(cd3);
		s.save(cd4);
		s.save(cd5);
		t.commit();

	}

}
