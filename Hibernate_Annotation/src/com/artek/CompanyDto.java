package com.artek;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import antlr.debug.Event;


@Entity
@Table(name="Company_Anno")
public class CompanyDto {
	@Id
	@Column(name="c_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int c_id;
	@Column(name="c_name")
	private String c_name;
	@OneToMany(fetch=FetchType.LAZY,targetEntity=EventDto.class,cascade=CascadeType.ALL)
	@JoinColumn(name="com_id", referencedColumnName="c_id")
	private Set c_event;
	public int getC_id() {
		return c_id;
	}
	public void setC_id(int c_id) {
		this.c_id = c_id;
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public Set getC_event() {
		return c_event;
	}
	public void setC_event(Set c_event) {
		this.c_event = c_event;
	}
	
	
} 
