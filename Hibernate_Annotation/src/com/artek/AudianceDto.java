package com.artek;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="audiance_anno")
public class AudianceDto {
@Id
@Column(name="a_id")
@GeneratedValue(strategy=GenerationType.AUTO)
private int a_id;

@Column(name="a_name")
private String a_name;

public int getA_id() {
	return a_id;
}

public void setA_id(int a_id) {
	this.a_id = a_id;
}

public String getA_name() {
	return a_name;
}

public void setA_name(String a_name) {
	this.a_name = a_name;
}

}
