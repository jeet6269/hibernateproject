package com.artek;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="event_Anno")
public class EventDto {
@Id
@Column(name="e_id")
@GeneratedValue(strategy=GenerationType.AUTO)
private int e_id;
@Column(name="e_name")
private String e_name;

@OneToMany(fetch=FetchType.LAZY,targetEntity=AudianceDto.class,cascade=CascadeType.ALL)
@JoinColumn(name="eve_id", referencedColumnName="e_id")
private Set Audiance;


@OneToOne(targetEntity=ManagerDto.class,cascade=CascadeType.ALL)
@JoinColumn(name="eventid", referencedColumnName="m_id")
private ManagerDto manager;

@OneToOne(targetEntity=SpeakerDto.class,cascade=CascadeType.ALL)
@JoinColumn(name="evenid", referencedColumnName="s_id")
private SpeakerDto speaker;




public SpeakerDto getSpeaker() {
	return speaker;
}
public void setSpeaker(SpeakerDto speaker) {
	this.speaker = speaker;
}
public Set getAudiance() {
	return Audiance;
}
public void setAudiance(Set audiance) {
	Audiance = audiance;
}
public ManagerDto getManager() {
	return manager;
}
public void setManager(ManagerDto manager) {
	this.manager = manager;
}
public int getE_id() {
	return e_id;
}
public void setE_id(int e_id) {
	this.e_id = e_id;
}
public String getE_name() {
	return e_name;
}
public void setE_name(String e_name) {
	this.e_name = e_name;
}


}
