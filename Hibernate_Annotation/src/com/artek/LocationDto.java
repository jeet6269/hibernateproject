package com.artek;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="location_anno")
public class LocationDto {
@Id
@Column(name="l_id")
@GeneratedValue(strategy=GenerationType.AUTO)
private int l_id;
@Column(name="l_name")
private String l_name;
@ManyToOne(cascade=CascadeType.ALL)
@JoinColumn(name="event_id", referencedColumnName="e_id")
private EventDto event;
public int getL_id() {
	return l_id;
}
public void setL_id(int l_id) {
	this.l_id = l_id;
}
public String getL_name() {
	return l_name;
}
public void setL_name(String l_name) {
	this.l_name = l_name;
}
public EventDto getEvent() {
	return event;
}
public void setEvent(EventDto event) {
	this.event = event;
}


}
