package com.artek;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Final_Event_Get {

	public static void get(Session s) {
	
	Transaction t =s.beginTransaction();
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter the company id");
	int cid = sc.nextInt();
	CompanyDto c = (CompanyDto) s.get(CompanyDto.class,cid);
	System.out.println("Company details"+"    "+c.getC_id()+"    "+c.getC_name()+"    ");
	Set set =c.getC_event();
	for (Object object : set) {
		EventDto e = (EventDto) object;
		int i = e.getE_id();
		System.out.println("Event details"+"      "  +e.getE_id()+"    "+e.getE_name());
		ManagerDto m = e.getManager();
		System.out.println("Manager details"+"   "+m.getM_id()+"     "+m.getM_name());
		SpeakerDto sp = e.getSpeaker();
		System.out.println("Speaker name"+"     "+sp.getS_id()+"     "+sp.getS_name());
		Set set1 = e.getAudiance();
		for (Object object2 : set1) {
			AudianceDto a = (AudianceDto) object2;
			System.out.println("Audiance details"+"     "+a.getA_id()+"    "+a.getA_name());
		}
		 LocationDto l = (LocationDto) s.get(LocationDto.class, i);
		 System.out.println("Location name"+"    "+l.getL_id()+"    "+l.getL_name());
	}
	
     
	}

}
