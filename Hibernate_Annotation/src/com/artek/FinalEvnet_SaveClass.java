package com.artek;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class FinalEvnet_SaveClass {

	public static void save(Session s) {
		
		Transaction t =s.beginTransaction();
		
		ManagerDto m = new ManagerDto();
		m.setM_name("Sumit");
		
		
		SpeakerDto sp = new SpeakerDto();
		sp.setS_name("Ritesh");
		
		AudianceDto a = new AudianceDto();
		a.setA_name("Shree Ram");
		
		AudianceDto a1 = new AudianceDto();
		a1.setA_name(" Ramarjuna");
		Set set = new HashSet();
		set.add(a);
		set.add(a1);
		
		EventDto e = new EventDto();
		e.setE_name("Training");
		e.setManager(m);
		e.setSpeaker(sp);
		e.setAudiance(set);
		Set set1 = new HashSet();
		set1.add(e);
		
		LocationDto l = new LocationDto();
		l.setL_name("MR10 Road");
		l.setEvent(e);
		
		
		CompanyDto c = new CompanyDto();
		c.setC_name("real mart");
		c.setC_event(set1);
		s.save(c);
		s.save(l);
		t.commit();
		s.close();
		
	}

}
