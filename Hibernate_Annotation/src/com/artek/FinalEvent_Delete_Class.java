package com.artek;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class FinalEvent_Delete_Class {

	public static void delete(Session s) {
		
		Transaction t =s.beginTransaction();
		CompanyDto c = new CompanyDto();
		c.setC_id(2);
		
		LocationDto l = new LocationDto();
		l.setL_id(1);
		
		
		
		ManagerDto m = new ManagerDto();
		m.setM_id(1);
		
		
		SpeakerDto sp = new SpeakerDto();
		sp.setS_id(1);
		
		EventDto e = new  EventDto();
		e.setE_id(1);
		
		AudianceDto a = new AudianceDto();
		a.setA_id(1);
		s.delete(l);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		s.delete(m);
		s.delete(sp);
		s.delete(a);
		s.delete(e);
		s.delete(c);
		
		t.commit();
		s.close();

	}

}
