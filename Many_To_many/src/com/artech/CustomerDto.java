package com.artech;

import java.util.Set;

public class CustomerDto {
private int c_id;
private String c_name,address;
private String mobile_no;
private Set customproduct;

public Set getCustomproduct() {
	return customproduct;
}
public void setCustomproduct(Set customproduct) {
	this.customproduct = customproduct;
}
public int getC_id() {
	return c_id;
}
public void setC_id(int c_id) {
	this.c_id = c_id;
}
public String getC_name() {
	return c_name;
}
public void setC_name(String c_name) {
	this.c_name = c_name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getMobile_no() {
	return mobile_no;
}
public void setMobile_no(String mobile_no) {
	this.mobile_no = mobile_no;
}


}
