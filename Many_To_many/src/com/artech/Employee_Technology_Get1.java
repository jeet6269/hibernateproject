package com.artech;

import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Employee_Technology_Get1 {
public static void main(String[] args) {
	SessionFactory sf = new Configuration().configure().buildSessionFactory();
	Session s = sf.openSession();
	Query q = s.createQuery("from EmployeeDto");
	List li = q.list();
	for (Object object : li) {
		EmployeeDto e = (EmployeeDto) object;
		  System.out.println(e.getE_id()+"     "+e.getE_name()+"      "+e.getDegree()+"    "+e.getYear());
		//System.out.println(object+"  ");
	Set set = e.getEmployee();
	for (Object object2 : set) {
		TechnologyDto t = (TechnologyDto) object2;
		System.out.println(t.getT_id()+"      " +t.getCoursename()+"      "+t.getDuration());
	}
	}
}
}
