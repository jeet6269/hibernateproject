package com.artech;

import java.util.Set;

public class TechnologyDto {
private int t_id;
private String coursename;
private int duration;
private Set technology;
public int getT_id() {
	return t_id;
}
public void setT_id(int t_id) {
	this.t_id = t_id;
}
public String getCoursename() {
	return coursename;
}
public void setCoursename(String coursename) {
	this.coursename = coursename;
}
public int getDuration() {
	return duration;
}
public void setDuration(int duration) {
	this.duration = duration;
}
public Set getTechnology() {
	return technology;
}
public void setTechnology(Set technology) {
	this.technology = technology;
}

}
