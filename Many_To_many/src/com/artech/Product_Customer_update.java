package com.artech;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Product_Customer_update {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		ProductDto p = new ProductDto();
		p.setP_id(1);
		p.setP_name("Mango bite");
		p.setP_price(50);
      CustomerDto c = new CustomerDto();
      c.setC_id(3);
      c.setC_name("krishna");
      c.setMobile_no("9998896325");
      c.setAddress("Anil nagar");
      Set set = new HashSet<>();
      set.add(c);
      p.setProductcustom(set);
      s.update(p);
      t.commit();
      s.close();
	}

}
