package com.artech;

import java.util.Set;

public class EmployeeDto {
private int e_id;
private String e_name, degree,year;
private Set employee;
public int getE_id() {
	return e_id;
}
public void setE_id(int e_id) {
	this.e_id = e_id;
}
public String getE_name() {
	return e_name;
}
public void setE_name(String e_name) {
	this.e_name = e_name;
}
public String getDegree() {
	return degree;
}
public void setDegree(String degree) {
	this.degree = degree;
}
public String getYear() {
	return year;
}
public void setYear(String year) {
	this.year = year;
}
public Set getEmployee() {
	return employee;
}
public void setEmployee(Set employee) {
	this.employee = employee;
}

}
