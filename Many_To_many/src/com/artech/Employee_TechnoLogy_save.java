package com.artech;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Employee_TechnoLogy_save {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s= sf.openSession();
		Transaction tx = s.beginTransaction();
		TechnologyDto t = new TechnologyDto();
		t.setCoursename("java core");
		t.setDuration(3);
		TechnologyDto t1 = new TechnologyDto();
		t1.setCoursename("C");
		t1.setDuration(2);
		EmployeeDto e = new EmployeeDto();
		e.setE_name("varsh");
		e.setDegree("BCA");
		e.setYear("2011");
		EmployeeDto e2 = new EmployeeDto();
		e2.setE_name("ram");
		e2.setDegree("EC");
		e2.setYear("2014");
		Set set = new HashSet();
		set.add(e);
		set.add(e2);
		t.setTechnology(set);
		t1.setTechnology(set);
		s.save(t);
		s.save(t1);
		tx.commit();
		s.close();
		

	}

}
