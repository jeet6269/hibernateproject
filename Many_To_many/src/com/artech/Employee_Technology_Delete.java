package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Employee_Technology_Delete {
	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s =sf.openSession();
		Transaction tx = s.beginTransaction();
		TechnologyDto t = (TechnologyDto) s.get(TechnologyDto.class, 4);
		s.delete(t);
		tx.commit();
		s.close();
	}
}
