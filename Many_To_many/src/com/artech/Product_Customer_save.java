package com.artech;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Product_Customer_save {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		ProductDto p = new ProductDto();
		p.setP_name("james");
		p.setP_price(50);
		ProductDto p2 = new ProductDto();
		p2.setP_name("Sugar");
		p2.setP_price(500);
		CustomerDto c = new CustomerDto();
		c.setC_name("nilesh");
		c.setAddress("ranjit Hanuman");
		c.setMobile_no("94248814589");
		CustomerDto c1 = new CustomerDto();
		c1.setC_name("mahesh");
		c1.setAddress("nanada nagar");
		c1.setMobile_no("94248812458");
		CustomerDto c2 = new CustomerDto();
		c2.setC_name("Surendra");
		c2.setAddress("mahesg nagar");
		c2.setMobile_no("9424256891");
		Set set = new HashSet();
		set.add(c);
		set.add(c1);
		set.add(c2);
		p.setProductcustom(set);
		p2.setProductcustom(set);
		s.save(p);
		s.save(p2);
		t.commit();
		s.close();
		
		
		

	}

}
