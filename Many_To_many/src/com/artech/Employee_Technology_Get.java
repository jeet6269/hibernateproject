package com.artech;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

public class Employee_Technology_Get {

	public static void main(String[] args) {
	SessionFactory sf = new Configuration().configure().buildSessionFactory();
	Session s = sf.openSession();
	Criteria crit = s.createCriteria(TechnologyDto.class);
	crit.add(Restrictions.eq("t_id", 1));
	List list = crit.list();
	for (Object object : list) {
	//System.out.println(object+"    ");	
		TechnologyDto t = (TechnologyDto) object;
		System.out.println(t.getT_id()+"      " +t.getCoursename()+"      "+t.getDuration());
	   Set set = t.getTechnology();
	   for (Object object2 : set) {
		   EmployeeDto e = (EmployeeDto) object2;
		   System.out.println(e.getE_id()+"     "+e.getE_name()+"      "+e.getDegree()+"    "+e.getYear());
		
	}
	}

	}

}
