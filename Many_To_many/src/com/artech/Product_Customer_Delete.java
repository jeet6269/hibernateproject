package com.artech;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Product_Customer_Delete {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		//ProductDto p = new ProductDto();
		//p.setP_id(3);
		//s.delete(p);
		CustomerDto c = new CustomerDto();
		c.setC_id(3);
		s.delete(c);
        t.commit();
        s.close();
	}

}
