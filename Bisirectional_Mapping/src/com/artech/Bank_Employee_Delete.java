package com.artech;

import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Bank_Employee_Delete {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t =s.beginTransaction();
		//many to one 
		/*EmployeeDto e = new EmployeeDto();
		e.setE_id(17);*/
		//one to many
       BankDto b = (BankDto) s.get(BankDto.class, 5);
       Set set = b.getBankclass();
       for (Object object : set) {
		EmployeeDto e = (EmployeeDto) object;
		e.setE_id(9);
	}
       s.delete(b);
		//s.delete(e);
		t.commit();
		s.close();
		

	}

}
