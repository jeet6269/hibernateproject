package com.artech;

public class CustomerDto {
private int c_id;
private String name,address;
private int commonid;
private VendorDto customers;

public int getCommonid() {
	return commonid;
}
public void setCommonid(int commonid) {
	this.commonid = commonid;
}
public int getC_id() {
	return c_id;
}
public void setC_id(int c_id) {
	this.c_id = c_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public VendorDto getCustomers() {
	return customers;
}
public void setCustomers(VendorDto customers) {
	this.customers = customers;
}

}
