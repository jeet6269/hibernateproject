package com.artech;

import java.util.Set;

public class VendorDto {
private int v_id;
private String v_name,productName;
private Set vendors;
public int getV_id() {
	return v_id;
}
public void setV_id(int v_id) {
	this.v_id = v_id;
}
public String getV_name() {
	return v_name;
}
public void setV_name(String v_name) {
	this.v_name = v_name;
}
public String getProductName() {
	return productName;
}
public void setProductName(String productName) {
	this.productName = productName;
}
public Set getVendors() {
	return vendors;
}
public void setVendors(Set vendors) {
	this.vendors = vendors;
}

}
