package com.artech;

public class EmployeeDto {
private int e_id;
private String e_name,address;
private BankDto bankemp;
private int comman;

public int getComman() {
	return comman;
}
public void setComman(int comman) {
	this.comman = comman;
}
public int getE_id() {
	return e_id;
}
public void setE_id(int e_id) {
	this.e_id = e_id;
}
public String getE_name() {
	return e_name;
}
public void setE_name(String e_name) {
	this.e_name = e_name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public BankDto getBankemp() {
	return bankemp;
}
public void setBankemp(BankDto bankemp) {
	this.bankemp = bankemp;
}


}
