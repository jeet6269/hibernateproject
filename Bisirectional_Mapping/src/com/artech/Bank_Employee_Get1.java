package com.artech;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.hibernate.hql.classic.WhereParser;

public class Bank_Employee_Get1 {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		
		Query q = s.createSQLQuery("Select * from e_table where bemp = 2");
		List<Object[]> l = q.list();
		for(Object[] obj:l)
		{
			for(Object obj1:obj)
			{
			System.out.print(obj1+"\t");
			}
			System.out.println();
		}
		}

}
