package com.artech;

import java.util.Scanner;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Bank_Employee_Update {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		//one to many
		/*BankDto b = (BankDto) s.get(BankDto.class,12);
		b.setBankname("bank of india");
		b.setAddress("Palasiya Squre");
		Set set = b.getBankclass();
		for (Object object : set) {
			EmployeeDto e = (EmployeeDto) object;
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the address");
			String st = sc.nextLine();
			System.out.println("Enter the name");
			String st1 = sc.next();
			e.setAddress(st);
            e.setE_name(st1);}*/
		
		//many to one
		EmployeeDto e = (EmployeeDto) s.get(EmployeeDto.class, 17);
		e.setE_name("ankur");
		e.setAddress("bangali Squre");
		
		BankDto b = e.getBankemp();
		b.setB_id(11);
		b.setBankname("ICICI bank");
		b.setAddress("malawa Parisr AB road");
		s.update(e);
		t.commit();
		s.close(); 
		

	}

}
