package com.artech;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Customer_vendor_get {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		//one to many
		Query q = s.createQuery("from VendorDto");
		//Query q = s.createQuery("from CustomerDto");
		List l = q.list();
				//one to many
		for (Object object : l) {
			VendorDto vd =(VendorDto) object;
			System.out.println(vd.getV_name()+"   "+vd.getProductName()+"  "+vd.getV_id());
			
			Set set = vd.getVendors();
			for (Object object2 : set) {
				CustomerDto cd = (CustomerDto) object2;
				System.out.println("customer"+"    "+cd.getName()+"   "+cd.getAddress()+"  "+cd.getC_id());
			}
			
		}
		
		//many to one
		
		/*for (Object object : l) {
			CustomerDto cd =(CustomerDto) object;
			VendorDto vd = cd.getCustomers();
			System.out.println("customer"+"    "+cd.getName()+"   "+cd.getAddress()+"  "+cd.getC_id());
			System.out.println("Vendor"+"   "+vd.getV_name()+"   "+vd.getProductName()+"  "+vd.getV_id());
		}*/
		}
		
		
        
	}


