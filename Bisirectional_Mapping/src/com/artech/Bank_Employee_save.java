package com.artech;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Bank_Employee_save {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		//many to one mapping
		BankDto b= new BankDto();
		b.setBankname("State Bank");
		b.setAddress("Vijay nagar road");
		EmployeeDto e = new EmployeeDto();
		e.setE_name("Rohan");
		e.setAddress("Geeta Bhawan");
		e.setBankemp(b);
		EmployeeDto e1 = new EmployeeDto();
		e1.setE_name("Sohan");
		e1.setAddress("palasiya");
		e1.setBankemp(b);
		//one to many
	    BankDto bd = new BankDto();
	    bd.setBankname("Bank of indore");
	    bd.setAddress("MR9 road");
	    EmployeeDto ed = new EmployeeDto();
	    ed.setE_name("Mohan");
	    ed.setAddress("bajrang nagar");
	    EmployeeDto ed1 = new EmployeeDto();
	    ed1.setE_name("geeta");
	    ed1.setAddress("Shyam nagar");
	    Set set = new HashSet();
	    set.add(ed);
	    set.add(ed1);
	    bd.setBankclass(set);
	    s.save(bd);
	    s.save(e);
	    s.save(e1);
	    t.commit();
	    s.close();
	    sf.close();
	}

}
