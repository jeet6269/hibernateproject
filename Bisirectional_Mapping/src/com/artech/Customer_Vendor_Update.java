package com.artech;

import java.util.Scanner;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Customer_Vendor_Update {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		   Session s = sf.openSession();
		   Transaction t = s.beginTransaction();
		   //one to many mapping
          /*VendorDto vd = (VendorDto) s.get(VendorDto.class, 1);
          vd.setV_name("RamGopal");
          Set set = vd.getVendors();
          for (Object object : set) {
        	  CustomerDto cd = (CustomerDto) object;
			Scanner sc = new Scanner(System.in);
					System.out.println("Enter the name");
			String st = sc.next();
			cd.setName(st);
			}*/
          //Many to one mapping
          VendorDto vd1 = new  VendorDto();
          vd1.setV_id(2);
          vd1.setV_name("suman");
          CustomerDto cd = new CustomerDto();
          cd.setAddress("vijay nagar");
          cd.setName("govind");
          cd.setCustomers(vd1);
         // s.save(vd);
          s.save(cd);
          t.commit();
          s.close();
	}

}
