package com.artech;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Customer_vendor_Demo {

	public static void main(String[] args) {
   SessionFactory sf = new Configuration().configure().buildSessionFactory();
   Session s = sf.openSession();
   Transaction t = s.beginTransaction();
   //System.out.println("done");

   VendorDto vd = new VendorDto();
   vd.setV_name("Sunil");
   vd.setProductName("wheel shope");
   CustomerDto cd = new CustomerDto();
   cd.setName("Ramu");
   cd.setAddress("rambaag");
   CustomerDto cd1 = new CustomerDto();
   cd1.setName("Kishan");
   cd1.setAddress("Krishnbaag");
   
 //one to many mapping
   
 /* Set set =new HashSet();
  set.add(cd);
  set.add(cd1);
  vd.setVendors(set);*/
  //Many To One
  cd.setCustomers(vd);
  cd1.setCustomers(vd);
  
  //s.save(vd);
  s.save(cd);
  s.save(cd1);
  t.commit();
  s.close();
	}

}
