package com.artech;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Customer_Vendor_Delete {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		//many to one
		CustomerDto cd = new CustomerDto();
		cd.setC_id(13);
		//one to many
		VendorDto vd =(VendorDto) s.get(VendorDto.class, 7);
		//s.delete(cd);
		s.delete(vd);
		t.commit();
		s.close();
	}

}
