package com.artech;

import java.util.Set;

public class BankDto {
private int b_id;
private String bankname,address;
private Set bankclass;
public int getB_id() {
	return b_id;
}
public void setB_id(int b_id) {
	this.b_id = b_id;
}
public String getBankname() {
	return bankname;
}
public void setBankname(String bankname) {
	this.bankname = bankname;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public Set getBankclass() {
	return bankclass;
}
public void setBankclass(Set bankclass) {
	this.bankclass = bankclass;
}

}
