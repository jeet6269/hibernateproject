package com.artech;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Query_Level_Cache1 {
public static void main(String[] args) {
	SessionFactory sf = new Configuration().configure().buildSessionFactory();
	for (int i = 0; i < 5; i++) {
	Session s = sf.openSession();
	Transaction t = s.beginTransaction();
	Query q = s.createQuery("from UserDTO where id =5");
	q.setCacheable(true);
	List li = q.list();
	Iterator itr = li.iterator();
	while(itr.hasNext())
	{
		UserDTO udt = (UserDTO) itr.next();
		//Object obj = itr.next();
		System.out.print(udt.getId());
		System.out.print("\t");
		System.out.print(udt.getFname());
		System.out.print("\t");
		System.out.print(udt.getLname());
		System.out.print("\t");
		System.out.println(udt.getAge());
		s.close();
	}
	
}
}
}
