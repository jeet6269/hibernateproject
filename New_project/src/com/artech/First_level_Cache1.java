package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class First_level_Cache1 {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		for (int i = 0; i < 10; i++) {
			UserDTO udt = (UserDTO) s.get(UserDTO.class, 20);
			System.out.print(udt.getId());
			System.out.print("\t");
			System.out.print(udt.getFname());
			System.out.print("\t");
			System.out.print(udt.getLname());
			System.out.print("\t");
			System.out.print(udt.getAge());
			System.out.print("\t");
			System.out.println(i);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}

}
