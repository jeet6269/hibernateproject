package com.artech;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class HQL_Query_Class {
public static void main(String[] args) {
	Configuration conf = new Configuration();
	conf.configure();
	SessionFactory sf = conf.buildSessionFactory();
	Session s = sf.openSession();
	Transaction t = s.beginTransaction();
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter the No 1 For Distinct");
	System.out.println("Enter the No 2 For And OR ");
	System.out.println("Enter the NO 3 For Limit");
	System.out.println("Enter the NO 4 For Order By");
	System.out.println("Enter the NO 5 For IN");
	System.out.println("Enter the NO 6 For Between");
	System.out.println("Enter the NO 7 For Group");
	System.out.println("Enter the NO 8 For Like");
	System.out.println("Enter the NO 9 For Having");
	System.out.println("Enter the NO 10 For union");
	System.out.println("Enter the NO 11 For union All");
	System.out.println("Enter the NO 12 For Concat Function");
	System.out.println("Enter the NO 11 For Lower case");
	System.out.println("Enter the NO 12 For Uper case");

	int number = sc.nextInt();
	switch (number) {
	case 1:
	{
		Query q = s.createQuery("select distinct u.fname from UserDTO u");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			Object obj =(Object) itr.next();
			System.out.print(obj);
			System.out.println(" ");
		}

	}
		
		break;
	case 2:
	{
		System.out.println("Enter the 1 for And");
		System.out.println("Enter the 2 for And");
		
		int no = sc.nextInt();
		if(no==1)
		{
		Query q = s.createQuery("from UserDTO u where u.age=18 and u.fname='Rahul'");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			UserDTO udt = (UserDTO) itr.next();
			System.out.print(udt.getAge()+" : ");
			System.out.print("\t");
			System.out.print(udt.getFname()+" : ");
			System.out.print("\t");
			System.out.print(udt.getId()+" : ");
			System.out.print("\t");
			System.out.println(udt.getLname()+" : ");
		
		}	
		}
		else
			if(no==2)
			{
				Query q = s.createQuery("from UserDTO u  where u.age=18 or u.lname='sharma'");
				List l = q.list();
				Iterator itr = l.iterator();
				while(itr.hasNext())
				{
					UserDTO udt = (UserDTO) itr.next();
					System.out.print(udt.getAge()+" : ");
					System.out.print("\t");
					System.out.print(udt.getFname()+" : ");
					System.out.print("\t");
					System.out.print(udt.getId()+" : ");
					System.out.print("\t");
					System.out.println(udt.getLname()+" : ");
				
				}
				
			}
		else {
			System.out.println("WRONG NUMBER SELECTED ");
		}
	}
	break;
	case 3:
	{
		Query q = s.createQuery("select u.age,u.fname from UserDTO u limit 3");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			Object[] obj =(Object[]) itr.next();
			for(Object obj1:obj)
			{
			System.out.print(obj1);
			}
			System.out.println(" ");
		}	
	}
	break;
	case 4:
	{
		Query q = s.createQuery("select u.age,u.fname,u.lname from UserDTO u order by u.fname");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			Object[] obj = (Object[])itr.next();
			for (Object object : obj) {
				System.out.print(object+"  ");
			}
			System.out.println(" ");
		}
		
	}
	break;
	case 5:
	{
		Query q = s.createQuery(" from UserDTO u where u.age IN(24,20,18)");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
		UserDTO udt = (UserDTO) itr.next();
		System.out.print(udt.getId());
		System.out.print("\t");
		System.out.print(udt.getFname());
		System.out.print("\t");
		System.out.print(udt.getLname());
		System.out.print("\t");
		System.out.print(udt.getEmail());
		System.out.print("\t");
		System.out.println(udt.getAge());
		}
	}
	break;
	case 6:
	{
		Query q = s.createQuery("select u.fname,u.lname from UserDTO u where u.id between 2 and 10 order by u.fname");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			Object[] obj = (Object[])itr.next();
			for (Object object : obj) {
				System.out.print(object+"  ");
			}
			System.out.println(" ");
		}
	}
	break;
	case 7:
	{
		Query q = s.createQuery("select u.fname,u.lname, count(*) from UserDTO u group by u.lname");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			Object[] obj = (Object[])itr.next();
			for (Object object : obj) {
				System.out.print(object+"  ");
			}
			System.out.println(" ");
		
		}
		}
    break;
	case 8:
	{
		Query q = s.createQuery("from UserDTO u where u.fname like 'R%'");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			UserDTO udt = (UserDTO) itr.next();
			System.out.print(udt.getId());
			System.out.print("\t");
			System.out.print(udt.getFname());
			System.out.print("\t");
			System.out.print(udt.getLname());
			System.out.print("\t");
			System.out.println(udt.getAge());
			
		}
	}
	break;
	case 9 :
	{
		Query q = s.createQuery("select u.fname,u.lname,u.age from UserDTO u group by u.age having age >18");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			Object[] obj =(Object[]) itr.next();
			for( Object obj1:obj)
			{
			System.out.print(obj1+"  ");
			}
			System.out.println("   ");
		}
				
	}
	break;
	case 10 :
	{
		Query q = s.createQuery("SELECT fname FROM UserDTO"
					+ " UNION"
					+ " SELECT fname FROM StaffClass");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			//UserDTO udt = (UserDTO) itr.next();
			Object obj = (Object)itr.next();
			System.out.print(obj+"  ");
			}
			System.out.println("   ");
			
	}
	break;
	case 11 :
	{
		Query q = s.createQuery("SELECT fname FROM UserDTO"
					+ " UNION all"
					+ " SELECT fname FROM StaffClass");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			//UserDTO udt = (UserDTO) itr.next();
			Object obj = (Object)itr.next();
			System.out.print(obj+"  ");
			}
			System.out.println("   ");
			
	}
	break;
	case 12:
	{
	Query q = s.createQuery("SELECT fname, CONCAT(id, ' ', fname, '  ', lname) AS FullName FROM StaffClass");
	List l = q.list();
	Iterator itr = l.iterator();
	while(itr.hasNext()){
		Object[] obj = (Object[])itr.next();
		for (Object object : obj) {
			System.out.print(object+"  ");
		}
		System.out.println("   ");
	}
	}
	break;
	case 13:
	{
	Query q = s.createQuery("SELECT LOWER(fname) AS fname FROM StaffClass");
	List l = q.list();
	Iterator itr = l.iterator();
	while(itr.hasNext()){
		/*Object[] obj = (Object[])itr.next();
		for (Object object : obj) {
			System.out.print(object+"  ");
		}*/
		Object obj =(Object) itr.next();
		System.out.println(obj+"  ");
	}
	}
	break;
	case 14:
	{
	Query q = s.createQuery("SELECT UPPER(fname) AS fname FROM StaffClass");
	List l = q.list();
	Iterator itr = l.iterator();
	while(itr.hasNext()){
		/*Object[] obj = (Object[])itr.next();
		for (Object object : obj) {
			System.out.print(object+"  ");
		}*/
		Object obj =(Object) itr.next();
		System.out.println(obj+"  ");
	}
	}
	break;
	default:
	{
		System.out.println("PLZ ENTER THE CORRECT NUMBER");
	}
		break;
	}
	s.close();
}
}
