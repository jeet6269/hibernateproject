package com.artech;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.hibernate.engine.query.ParameterParser;

public class Hql_DML_Query {
public static void main(String[] args) throws Exception
{
	Configuration conf = new Configuration();
	conf.configure();
	SessionFactory sf = conf.buildSessionFactory();
	Session s= sf.openSession();
	Transaction t = s.beginTransaction();
	Scanner sc = new  Scanner(System.in);
    System.out.println("Enter the the 1 for delete any records");
    System.out.println("Enter the the 2 for update any records");
    int number = sc.nextInt();
	if(number==1)
	{
	System.out.println("Enter the id ");
	int i = sc.nextInt();
	Query q = s.createQuery("delete from StaffClass where id ="+i);
	StaffClass sc1 = new StaffClass();
		q.executeUpdate();
		t.commit();
	}
	else if (number==2)
	{
		System.out.println("Enter the id ");
		int i1 = sc.nextInt();
		Query q = s.createQuery("update StaffClass set fname='ramarjuna', lname ='aayar' where id ="+i1);
		StaffClass sc1 = new StaffClass();
			q.executeUpdate();
			t.commit();
	}
		s.close();
	}
	}
