package com.artech;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class SQL_Query_Class {
public static void main(String[] args) {
	Configuration conf = new Configuration();
	conf.configure();
	SessionFactory sf = conf.buildSessionFactory();
	Session s = sf.openSession();
	Transaction t = s.beginTransaction();
	Scanner sc =new Scanner(System.in);
	System.out.println("Enter the no 1 for Distinct");
	System.out.println("Enter the no 2 for And and Or");
	System.out.println("Enter the no 3 limit ");
	System.out.println("Enter the no 4 for ORder by");
	System.out.println("Enter the no 5 for IN");
	System.out.println("Enter the no 6 for Between");
	System.out.println("Enter the no 7 for Unino");
	System.out.println("Enter the no 8 for Group");
	System.out.println("Enter the no 9 for Like ");
	System.out.println("Enter the no 10 for Having ");
	System.out.println("Enter the no 11 for union all ");
	System.out.println("Enter the no 12 for select into inseart ");
	System.out.println("Enter the no 13 for select Concat Function");
	System.out.println("Enter the no 14 for select Lower case ");
	System.out.println("Enter the no 15 for select Uper case");
	
	int number =sc.nextInt(); 
	//int number = 0;
	switch (number) {
	case 1:
	{
		Query q = s.createSQLQuery("SELECT DISTINCT LastName FROM UserDTO_table");
		List l = q.list();
		Iterator itr = l.iterator();
		while (itr.hasNext()) {
			Object object = (Object) itr.next();
			System.out.println(object);
			}}
		break;
		case 2:
		{
		//	Query q = s.createSQLQuery("SELECT FirstName, LastName FROM UserDTO_table WHERE ID=5 AND Age=23");
			Query q = s.createSQLQuery("SELECT FirstName, LastName FROM UserDTO_table WHERE ID=5 OR Age=24");
			List l = q.list();
			Iterator itr = l.iterator();
			while (itr.hasNext()) {
				Object[] object = (Object[]) itr.next();
				for (Object object2 : object) {
					System.out.print("   "+object2);
				}
				System.out.println(" ");
			}
		}
break;
		case 3:
		{
			Query q = s.createSQLQuery("SELECT id,FirstName, LastName FROM UserDTO_table  LIMIT 4");
			List l = q.list();
			Iterator itr = l.iterator();
			while (itr.hasNext()) {
				Object[] object = (Object[]) itr.next();
				for (Object object2 : object) {
					System.out.print("   "+object2);
				}
				System.out.println("  ");
				}
			
		}
		break;
		case 4:
		{
			Query q = s.createSQLQuery("SELECT id,FirstName, LastName FROM UserDTO_table ORDER BY id");
			List l = q.list();
			Iterator itr = l.iterator();
			while (itr.hasNext()) {
				Object[] object = (Object[]) itr.next();
				for (Object object2 : object) {
					System.out.print("   "+object2);
				}
				System.out.println("  ");
				}
		}
		break;
		case 5:
		{
			Query q = s.createSQLQuery("SELECT * FROM UserDTO_table WHERE LastName IN ('sharma','tiwari','mishra')");
			List l = q.list();
			Iterator itr = l.iterator();
			while (itr.hasNext()) {
				Object[] object = (Object[]) itr.next();
				for (Object object2 : object) {
					System.out.print("   "+object2);
				}
				System.out.println("  ");
				}
		}
		break;
		case 6:
		{
			Query q = s.createSQLQuery("SELECT * FROM UserDTO_table WHERE id BETWEEN 1 AND 9");
			List l = q.list();
			Iterator itr = l.iterator();
			while (itr.hasNext()) {
				Object[] object = (Object[]) itr.next();
				for (Object object2 : object) {
					System.out.print("   "+object2);
				}
				System.out.println("  ");
				}
		}
		break;
		case 7:
		{
			Query q = s.createSQLQuery("SELECT FirstName FROM userdto_table"
					+ " UNION"
					+ " SELECT fname FROM staff_table ");
			List l = q.list();
			Iterator itr = l.iterator();
			while (itr.hasNext()) {
				Object object = (Object) itr.next();
				System.out.print(object+"  ");
				System.out.println("");
				}
		}
		break;
		case 8:
		{
		Query q = s.createSQLQuery(" SELECT LastName,COUNT(*) FROM userdto_table GROUP BY LastName");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext()){
			Object[] obj = (Object[])itr.next();
			for (Object object : obj) {
				System.out.print(object+"  ");
			}
			System.out.println("   ");
		}
		}
		break;
		case 9:
		{
		//Query q = s.createSQLQuery(" SELECT FirstName,LastName FROM userdto_table WHERE LastName LIKE 'k%s'");
			//Query q = s.createSQLQuery(" SELECT FirstName,LastName FROM userdto_table WHERE FirstName LIKE 'j%'");
			//Query q = s.createSQLQuery(" SELECT FirstName,LastName FROM userdto_table WHERE FirstName LIKE '_%a'");
			Query q = s.createSQLQuery(" SELECT FirstName,LastName FROM userdto_table WHERE FirstName LIKE '_%a'");
			List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext()){
			Object[] obj = (Object[])itr.next();
			for (Object object : obj) {
				System.out.print(object+"  ");
			}
			System.out.println("   ");
		}
		}
		break;
		case 10:
		{
			Query q = s.createSQLQuery("SELECT * FROM userdto_table GROUP BY age HAVING age >18");
			List l = q.list();
			Iterator itr = l.iterator();
			while(itr.hasNext())
			{
				Object[] obj =(Object[]) itr.next();
				for( Object obj1:obj)
				{
				System.out.print(obj1+"  ");
				}
				System.out.println("   ");
			}
					
		}
		break;
		case 11:
		{
			Query q = s.createSQLQuery("SELECT FirstName FROM userdto_table"
					+ " UNION All"
					+ " SELECT fname FROM staff_table ");
			List l = q.list();
			Iterator itr = l.iterator();
			while (itr.hasNext()) {
				Object object = (Object) itr.next();
				System.out.print(object+"  ");
				System.out.println("");
				}
		}
		break;
		case 12:
		{
			Query q = s.createSQLQuery("INSERT INTO New_table(id,fname,lname) SELECT id,fname,lname from staff_table where id >7");
			
			int i =	q.executeUpdate();
			System.out.println(i);
			
		}
		break;
		case 13:
		{
		Query q = s.createSQLQuery("SELECT fname, CONCAT(id, ' ', fname, '  ', lname) AS FullName FROM staff_table;");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext()){
			Object[] obj = (Object[])itr.next();
			for (Object object : obj) {
				System.out.print(object+"  ");
			}
			System.out.println("   ");
		}
		}
		break;
		case 14:
		{
		Query q = s.createSQLQuery("SELECT LOWER(fname) AS fname FROM staff_table");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext()){
			/*Object[] obj = (Object[])itr.next();
			for (Object object : obj) {
				System.out.print(object+"  ");
			}*/
			Object obj =(Object) itr.next();
			System.out.println(obj+"  ");
		}
		}
		break;
		case 15:
		{
		Query q = s.createSQLQuery("SELECT UPPER(fname) AS fname FROM staff_table");
		List l = q.list();
		Iterator itr = l.iterator();
		while(itr.hasNext()){
			/*Object[] obj = (Object[])itr.next();
			for (Object object : obj) {
				System.out.print(object+"  ");
			}*/
			Object obj =(Object) itr.next();
			System.out.println(obj+"  ");
		}
		}
		break;
		default:
		{
			System.out.println("PLZ ENTER THE CORRECT NUMBER");
		}
	}
	s.close();
}
}
