package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Second_Level_cache {

	public static void main(String[] args) {
	SessionFactory sf = new  Configuration().configure().buildSessionFactory();
	
	//Transaction t = s.beginTransaction();
	for (int i = 0; i < 5; i++) {
		Session s = sf.openSession();
		UserDTO udt = (UserDTO) s.load(UserDTO.class, 4);
		  System.out.print(udt.getId());
			System.out.print("\t");
			System.out.print(udt.getFname());
			System.out.print("\t");
			System.out.print(udt.getLname());
			System.out.print("\t");
			System.out.print(udt.getAge());
			System.out.print("\t");
			System.out.println(i);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		s.close();
	}
		Session s1 = sf.openSession();
		UserDTO udt1 = (UserDTO) s1.get(UserDTO.class, 4);
		  System.out.print(udt1.getId());
			System.out.print("\t");
			System.out.print(udt1.getFname());
			System.out.print("\t");
			System.out.print(udt1.getLname());
			System.out.print("\t");
			System.out.println(udt1.getAge());
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		s1.close();
		Session s2 = sf.openSession();
		UserDTO udt2 = (UserDTO) s2.get(UserDTO.class, 4);
		  System.out.print(udt2.getId());
			System.out.print("\t");
			System.out.print(udt2.getFname());
			System.out.print("\t");
			System.out.print(udt2.getLname());
			System.out.print("\t");
			System.out.println(udt2.getAge());
		s2.close();
		}
	}
	


