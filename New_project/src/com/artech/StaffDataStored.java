package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class StaffDataStored  
{
public static void main(String[] args)throws Exception {
	//creat the configration object
	Configuration conf = new Configuration();
	
	conf.configure();
	//creat the session factory object
	SessionFactory sfc= conf.buildSessionFactory();
	//creat the sission object
	Session session=sfc.openSession();
	//creat the transaction object
	Transaction t = session.beginTransaction();
	StaffClass sc = new StaffClass();
	
	sc.setfname("ram");
	sc.setlname("Sharma");
	session.save(sc);
	t.commit();
	
	session.close();
	
}
}
