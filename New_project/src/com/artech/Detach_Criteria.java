package com.artech;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

public class Detach_Criteria {

	public static void main(String[] args)
	{
	SessionFactory sf = new Configuration().configure().buildSessionFactory();
	Session s = sf.openSession();
	Transaction t = s.beginTransaction();
	DetachedCriteria dcrit = DetachedCriteria.forClass(UserDTO.class);
	dcrit.add(Restrictions.eq("lname", "sharma"));
	Criteria crit = dcrit.getExecutableCriteria(s);
	System.out.println(crit.hashCode());
	crit.setCacheable(true);
	List li = crit.list();
	Iterator itr = li.iterator();
	while(itr.hasNext())
	{
		
	UserDTO udt = (UserDTO) itr.next();
	System.out.print(udt.getId());
	System.out.print("\t");
	System.out.print(udt.getFname());
	System.out.print("\t");
	System.out.print(udt.getLname());
	System.out.print("\t");
	System.out.println(udt.getAge());
	}
	s.close();
	Session s1= sf.openSession();
	Criteria crit1 = dcrit.getExecutableCriteria(s1);
	System.out.println(crit1.hashCode());
	List li1 =  crit1.list();
	Iterator itr1 = li1.iterator();
	while(itr1.hasNext())
	{
	UserDTO udt = (UserDTO) itr1.next();
	System.out.print(udt.getId());
	System.out.print("\t");
	System.out.print(udt.getFname());
	System.out.print("\t");
	System.out.print(udt.getLname());
	System.out.print("\t");
	System.out.println(udt.getAge());
	}
	
	}
} 
