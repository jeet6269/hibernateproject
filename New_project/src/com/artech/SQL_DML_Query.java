package com.artech;

import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class SQL_DML_Query {
	public static void main(String[] args) throws Exception
	{
		Configuration conf = new Configuration();
		conf.configure();
		SessionFactory sf = conf.buildSessionFactory();
		Session s= sf.openSession();
		Transaction t = s.beginTransaction();
		Scanner sc = new  Scanner(System.in);
	    System.out.println("Enter the the 1 for delete any records");
	    System.out.println("Enter the the 2 for update any records");
	    int number = sc.nextInt();
		if(number==1)
		{
		System.out.println("Enter the id ");
		int i = sc.nextInt();
		Query q = s.createSQLQuery("delete from staff_table where id ="+i);
		StaffClass sc1 = new StaffClass();
			q.executeUpdate();
			t.commit();
		}
		else if (number==2)
		{
			System.out.println("Enter the id ");
			int i1 = sc.nextInt();
			Query q = s.createSQLQuery("update staff_table set fname='ramarjuna', lname ='aayar' where id ="+i1);
			StaffClass sc1 = new StaffClass();
				q.executeUpdate();
				t.commit();
		}
			s.close();
		}
}
