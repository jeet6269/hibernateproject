package com.artech;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Named_Query {
public static void main(String[] args) {
	SessionFactory sf = new Configuration().configure().buildSessionFactory();
	Session s = sf.openSession();
	//Transaction t = s.beginTransaction();
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter the number");
	int number = sc.nextInt();
	switch (number) {
	case 1:
	{
		Query q =s.getNamedQuery("Name Query");

		List li = q.list();
		Iterator itr = li.iterator();
		while(itr.hasNext())
		{
			//Object obj = itr.next();
			//System.out.println(obj);
			UserDTO udt = (UserDTO) itr.next();
			System.out.print(udt.getId());
			System.out.print("\t");
			System.out.print(udt.getFname());
			System.out.print("\t");
			System.out.print(udt.getLname());
			System.out.print("\t");
			System.out.println(udt.getAge());
		}
	}
		
		break;
	case 2:
	{
		Query q = s.getNamedQuery("New Name Query");
		q.setParameter("0", 2);
		q.setParameter("1", 4);
        q.setParameter("2", "rahul");
       
		List li = q.list();
		Iterator itr = li.iterator();
		while(itr.hasNext())
		{
			UserDTO udt = (UserDTO) itr.next();
			System.out.print(udt.getId());
			System.out.print("\t");
			System.out.print(udt.getFname());
			System.out.print("\t");
			System.out.print(udt.getLname());
			System.out.print("\t");
			System.out.println(udt.getAge());
		}
		
	}
	default:
		break;
	}
	
}
}
