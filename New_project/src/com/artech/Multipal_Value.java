package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Multipal_Value {
public static void main(String[] args) {
	Configuration conf = new Configuration();
	conf.configure();
	SessionFactory sf = conf.buildSessionFactory();
	Session s = sf.openSession();
	Transaction t = s.beginTransaction();
	//StaffClass sc= new StaffClass();
	StaffClass sc1 = new StaffClass();
			sc1.setfname("Sohan");
			sc1.setlname("Varma");
			StaffClass sc2 = new StaffClass();
			sc2.setfname("Mohan");
			sc2.setlname("Tiwari");
   StaffClass[] sc= new StaffClass[]{sc1,sc2};
    for (StaffClass s1 : sc) {
		s.save(s1);
		
	}
    t.commit();
    s.close();
}
}
