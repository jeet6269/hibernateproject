package com.artech;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class Criteria_Query_Class {

	public static void main(String[] args) {
		Configuration conf = new Configuration().configure();
		SessionFactory sf = conf.buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number ");
		int number =sc.nextInt();
		Criteria crit = s.createCriteria(UserDTO.class);
		if(number==1)
		{
		crit.add(Restrictions.eq("fname", "jitendra"));
		List l = crit.list();
		Iterator itr = l.iterator();
		while(itr.hasNext())
		{
			UserDTO udt = (UserDTO) itr.next();
			//Object obj= itr.next();
			//System.out.println(obj);
			System.out.println(udt.getId());
			System.out.println(udt.getFname());
			System.out.println(udt.getLname());
			System.out.println(udt.getAge());
			
		}
		}
		else if (number==2) {
			crit.add(Restrictions.gt("age", 18));
			List l = crit.list();
			Iterator itr = l.iterator();
			while(itr.hasNext())
			{
				UserDTO udt = (UserDTO) itr.next();
		    System.out.print(udt.getId());
			System.out.print("\t");
			System.out.print(udt.getFname());
			System.out.print("\t");
			System.out.print(udt.getLname());
			System.out.print("\t");
			System.out.println(udt.getAge());
		}
				
			}
		else if (number==3) 
		{
			//crit.addOrder(Order.asc("age"));
			crit.addOrder(Order.desc("age"));
			List l = crit.list();
			Iterator itr = l.iterator();
			while(itr.hasNext())
			{
				UserDTO udt = (UserDTO) itr.next();
				//Object obj = itr.next();
				
				 System.out.print(udt.getId());
					System.out.print("\t");
					System.out.print(udt.getFname());
					System.out.print("\t");
					System.out.print(udt.getLname());
					System.out.print("\t");
					System.out.println(udt.getAge());
			}
		}
		else if (number==4) {
			crit.add(Restrictions.le("age", 20));
			List l = crit.list();
			Iterator itr = l.iterator();
			while(itr.hasNext())
			{
				UserDTO udt = (UserDTO) itr.next();
			    System.out.print(udt.getId());
				System.out.print("\t");
				System.out.print(udt.getFname());
				System.out.print("\t");
				System.out.print(udt.getLname());
				System.out.print("\t");
				System.out.println(udt.getAge());
			}
		}
		else if (number==5) {
			//crit.add(Restrictions.ilike("fname", "j%"));
			//crit.add(Restrictions.ilike("fname", "r%a"));
			crit.add(Restrictions.ilike("lname", "%a"));
			List l = crit.list();
			Iterator itr = l.iterator();
			while(itr.hasNext())
			{
				UserDTO udt = (UserDTO) itr.next();
			    System.out.print(udt.getId());
				System.out.print("\t");
				System.out.print(udt.getFname());
				System.out.print("\t");
				System.out.print(udt.getLname());
				System.out.print("\t");
				System.out.println(udt.getAge());
				}}
		else if (number==6) {
			crit.add(Restrictions.between("age", 20, 24));
			List l = crit.list();
			Iterator itr = l.iterator();
			while(itr.hasNext())
			{
				UserDTO udt = (UserDTO) itr.next();
			    System.out.print(udt.getId());
				System.out.print("\t");
				System.out.print(udt.getFname());
				System.out.print("\t");
				System.out.print(udt.getLname());
				System.out.print("\t");
				System.out.println(udt.getAge());
			}
		}
		
		else if (number==7) {
			crit.add(Restrictions.eq("fname", "jitendra"));
			crit.add(Restrictions.eq("age", 24));
			List l = crit.list();
			Iterator itr = l.iterator();
			while(itr.hasNext())
			{
				UserDTO udt = (UserDTO) itr.next();
				    System.out.print(udt.getId());
					System.out.print("\t");
					System.out.print(udt.getFname());
					System.out.print("\t");
					System.out.print(udt.getLname());
					System.out.print("\t");
					System.out.println(udt.getAge());
			}
			}
		else if (number==8) {
			crit.add(Restrictions.or(Restrictions.eq("lname", "sharma"), Restrictions.eq("age", 24)));
			List l = crit.list();
			Iterator itr = l.iterator();
			while(itr.hasNext())
			{
				UserDTO udt = (UserDTO) itr.next();
				    System.out.print(udt.getId());
					System.out.print("\t");
					System.out.print(udt.getFname());
					System.out.print("\t");
					System.out.print(udt.getLname());
					System.out.print("\t");
					System.out.println(udt.getAge());
			}

		}
		else if (number==9) {
			crit.setMaxResults(6);
			List<Object> l = crit.list();
			Iterator<Object> itr = l.iterator();
			while(itr.hasNext())
			{
			UserDTO udt = (UserDTO) itr.next();
			 System.out.print(udt.getId());
				System.out.print("\t");
				System.out.print(udt.getFname());
				System.out.print("\t");
				System.out.print(udt.getLname());
				System.out.print("\t");
				System.out.println(udt.getAge());
		}
			}
		else if(number==10)
		{
			Object[] obj={18,20};
			crit.add(Restrictions.in("age", obj));
			List<Object> l = crit.list();
			Iterator<Object> itr = l.iterator();
			while(itr.hasNext())
			{
			UserDTO udt = (UserDTO) itr.next();
			 System.out.print(udt.getId());
				System.out.print("\t");
				System.out.print(udt.getFname());
				System.out.print("\t");
				System.out.print(udt.getLname());
				System.out.print("\t");
				System.out.println(udt.getAge());
		}
		}
		
		else {
			System.out.println("WRONG NO CHOOSE BY YOU ");
			System.out.println("PLZ ENTER THE CORRECT");
			
			}
		
		s.close();
		}
			}
		
	


