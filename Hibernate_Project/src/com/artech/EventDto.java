package com.artech;

import java.util.Set;

public class EventDto {
private int e_id;
private String e_name;
private int e_Comman;
private ManagerDto e_manager;
private SpeakerDto e_speaker;
private Set e_audiance;

public Set getE_audiance() {
	return e_audiance;
}
public void setE_audiance(Set e_audiance) {
	this.e_audiance = e_audiance;
}
public SpeakerDto getE_speaker() {
	return e_speaker;
}
public void setE_speaker(SpeakerDto e_speaker) {
	this.e_speaker = e_speaker;
}
public ManagerDto getE_manager() {
	return e_manager;
}
public void setE_manager(ManagerDto e_manager) {
	this.e_manager = e_manager;
}
public int getE_id() {
	return e_id;
}
public void setE_id(int e_id) {
	this.e_id = e_id;
}

public String getE_name() {
	return e_name;
}
public void setE_name(String e_name) {
	this.e_name = e_name;
}
public int getE_Comman() {
	return e_Comman;
}
public void setE_Comman(int e_Comman) {
	this.e_Comman = e_Comman;
}

}
