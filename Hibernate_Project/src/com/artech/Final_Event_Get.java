package com.artech;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Final_Event_Get {

	public static void get(Session s ) {
	
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter the Company id");
	int cid = sc.nextInt();
	CompanyDto c = (CompanyDto) s.get(CompanyDto.class,cid);
	System.out.println("Company details"+"    "+c.getC_id()+"    "+c.getC_name()+"    ");
	Set set =c.getCompany();
	for (Object object : set) {
		EventDto e = (EventDto) object;
		int i = e.getE_id();
		System.out.println("Event details"+"      "  +e.getE_id()+"    "+e.getE_name());
		ManagerDto m = e.getE_manager();
		System.out.println("Manager details"+"   "+m.getM_id()+"     "+m.getM_name());
		SpeakerDto sp = e.getE_speaker();
		System.out.println("Speaker name"+"     "+sp.getS_id()+"     "+sp.getS_name());
		Set set1 = e.getE_audiance();
		for (Object object2 : set1) {
			AudianceDto a = (AudianceDto) object2;
			System.out.println("Audiance details"+"     "+a.getA_id()+"    "+a.getA_name());
		}
		 LocationDto l = (LocationDto) s.get(LocationDto.class, i);
		 System.out.println("Location name"+"    "+l.getL_id()+"    "+l.getL_name()+"    "+l.getCity());
	}
	
     
	}

}
