package com.artech;

public class LocationDto {
private int l_id;
private String l_name,city;
 private EventDto e_location;
public int getL_id() {
	return l_id;
}
public void setL_id(int l_id) {
	this.l_id = l_id;
}
public String getL_name() {
	return l_name;
}
public void setL_name(String l_name) {
	this.l_name = l_name;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public EventDto getE_location() {
	return e_location;
}
public void setE_location(EventDto e_location) {
	this.e_location = e_location;
}
 
}
