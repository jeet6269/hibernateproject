package com.artech;

public class AudianceDto {
private int a_id;
private String a_name;
private int a_comman;
public int getA_id() {
	return a_id;
}
public void setA_id(int a_id) {
	this.a_id = a_id;
}
public String getA_name() {
	return a_name;
}
public void setA_name(String a_name) {
	this.a_name = a_name;
}
public int getA_comman() {
	return a_comman;
}
public void setA_comman(int a_comman) {
	this.a_comman = a_comman;
}

}
