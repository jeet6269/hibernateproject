package com.artech;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Customer_Vander_Save {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Vendor_Pojo vp = new Vendor_Pojo();
		//parents object
	/*	vp.setV_id(01);
		vp.setProduct_name("bikers");*/
		
		//child object
		Customer_Pojo cp = new Customer_Pojo();
		//cp.setC_id(101);
		cp.setFname("jitendra");
		cp.setLname("Sharma");
		cp.setCommon_id(1);
		Customer_Pojo cp1 = new Customer_Pojo();
		//cp1.setC_id(102);
		cp1.setFname("Shyam");
		cp1.setLname("Sharma");
		cp.setCommon_id(1);
		Customer_Pojo cp2 = new Customer_Pojo();
		//cp2.setC_id(103);
		cp2.setFname("Ram");
		cp2.setLname("Sharma");
		cp.setCommon_id(1);
		Customer_Pojo cp3 = new Customer_Pojo();
		//cp3.setC_id(104);
		cp.setCommon_id(1);
		cp3.setFname("Gopal");
		cp3.setLname("Tiwari");
		Set set = new HashSet();
		set.add(cp);
		set.add(cp1);
		set.add(cp2);
		set.add(cp3);
		//vp.setChild(set);
		Transaction t = s.beginTransaction();
		s.save(cp);
		s.save(cp1);
		s.save(cp2);
		s.save(cp3);
		t.commit();
		System.out.println("Your mapping done");
		s.close();
		
	}

}
