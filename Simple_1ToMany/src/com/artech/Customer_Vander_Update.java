package com.artech;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Customer_Vander_Update {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Vendor_Pojo vp = new Vendor_Pojo();
		//parents object
		vp.setV_id(01);
		vp.setProduct_name("bikers");
		
		//child object
		Customer_Pojo cp = new Customer_Pojo();
		cp.setC_id(1);
		cp.setFname("sonu");
		cp.setLname("Sharma");
		Customer_Pojo cp1 = new Customer_Pojo();
		cp1.setC_id(2);
		cp1.setFname("raj");
		cp1.setLname("kumar");
		Customer_Pojo cp2 = new Customer_Pojo();
		cp2.setC_id(3);
		cp2.setFname("Saurbha");
		cp2.setLname("sing");
		Customer_Pojo cp3 = new Customer_Pojo();
		cp3.setC_id(4);
		cp3.setFname("sumitra");
		cp3.setLname("Tiwari");
		Set set = new HashSet();
		set.add(cp);
		set.add(cp1);
		set.add(cp2);
		set.add(cp3);
		vp.setChild(set);
		Transaction t = s.beginTransaction();
		s.update(vp);
		t.commit();
		System.out.println("Your mapping done");
		s.close();
		

	}

}
