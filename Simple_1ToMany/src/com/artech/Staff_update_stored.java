package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Staff_update_stored {
public static void main(String[] args) throws Exception
{
	//creat the configration object
		Configuration conf = new Configuration();
		
		conf.configure("hibernate.cfg.xml");
		//creat the session factory object
		SessionFactory sfc= conf.buildSessionFactory();
		//creat the sission object
		Session session=sfc.openSession();
		//creat the transaction object
		Transaction t = session.beginTransaction();
		Staff_Class sc= new Staff_Class();
		sc.setid(4);
		sc.setfname("shyam");
		sc.setlname("Sharma");
		session.update(sc);
		t.commit();
		
		session.close();
		
	}
	}

