package com.artech;

import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Customer_Vandor_Get {

	public static void main(String[] args) 
	{
	SessionFactory sf = new Configuration().configure().buildSessionFactory();
			Session s = sf.openSession();
			Transaction t = s.beginTransaction();
			Vendor_Pojo vp =(Vendor_Pojo) s.get(Vendor_Pojo.class, 1);
			System.out.println(vp.getV_id()+"  "+vp.getProduct_name());
			
			Set set = vp.getChild();
			for (Object object : set) {
				//System.out.println(object);
				Customer_Pojo cp = (Customer_Pojo) object;
				System.out.println(cp.getC_id()+"  "+cp.getFname()+" "+cp.getLname());
				
			}
			s.close();
	}

}
