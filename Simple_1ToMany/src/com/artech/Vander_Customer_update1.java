package com.artech;

import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Vander_Customer_update1 {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		Vendor_Pojo vp = (Vendor_Pojo) s.get(Vendor_Pojo.class, 1);
		Set set =  vp.getChild();
		Customer_Pojo cp = new Customer_Pojo();
		cp.setFname("Romio");
		cp.setLname("tiwari");
		set.add(cp);
		vp.setChild(set);
		s.update(vp);
		t.commit();
		s.close();
		
	}

}
