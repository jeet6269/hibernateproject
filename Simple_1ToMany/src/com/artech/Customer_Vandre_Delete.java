package com.artech;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Customer_Vandre_Delete {

	public static void main(String[] args) {
	SessionFactory sf = new Configuration().configure().buildSessionFactory();
	Session s = sf.openSession();
	Transaction t = s.beginTransaction();
	Vendor_Pojo vp =(Vendor_Pojo) s.get(Vendor_Pojo.class, 2);
	s.delete(vp);
	t.commit();
	s.close();
	} 

}
