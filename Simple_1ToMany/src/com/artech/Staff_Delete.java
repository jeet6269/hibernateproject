package com.artech;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Staff_Delete {
public static void main(String[] args)
{
Configuration conf=	new Configuration();
conf.configure();
SessionFactory sf = conf.buildSessionFactory();
Session s = sf.openSession();
Transaction t = s.beginTransaction();
Staff_Class sc = new Staff_Class();
sc.setid(5);
s.delete(sc);
t.commit();
s.close();
}
}
