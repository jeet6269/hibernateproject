package com.artech1;

public class Customer_TX_Class {
	private int C_id;
private String C_name,C_email,C_address;
private int tx_id;
public int getC_id() {
	return C_id;
}
public void setC_id(int c_id) {
	C_id = c_id;
}
public String getC_name() {
	return C_name;
}
public void setC_name(String c_name) {
	C_name = c_name;
}
public String getC_email() {
	return C_email;
}
public void setC_email(String c_email) {
	C_email = c_email;
}
public String getC_address() {
	return C_address;
}
public void setC_address(String c_address) {
	C_address = c_address;
}
public int getTx_id() {
	return tx_id;
}
public void setTx_id(int tx_id) {
	this.tx_id = tx_id;
}

}
