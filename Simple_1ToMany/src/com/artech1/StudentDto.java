package com.artech1;

import java.util.Set;

public class StudentDto {
private int stud_id;
private  String fname,lname,address;
private int  D_id;
public int getStud_id() {
	return stud_id;
}
public void setStud_id(int stud_id) {
	this.stud_id = stud_id;
}
public String getFname() {
	return fname;
}
public void setFname(String fname) {
	this.fname = fname;
}
public String getLname() {
	return lname;
}
public void setLname(String lname) {
	this.lname = lname;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public int getD_id() {
	return D_id;
}
public void setD_id(int d_id) {
	D_id = d_id;
}

}
