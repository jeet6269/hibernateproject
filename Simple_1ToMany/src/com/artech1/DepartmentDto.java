package com.artech1;

import java.util.Set;

public class DepartmentDto {
private int D_id;
private String D_name,D_head;
private Set department;
public int getD_id() {
	return D_id;
}
public void setD_id(int d_id) {
	D_id = d_id;
}
public String getD_name() {
	return D_name;
}
public void setD_name(String d_name) {
	D_name = d_name;
}
public String getD_head() {
	return D_head;
}
public void setD_head(String d_head) {
	D_head = d_head;
}
public Set getDepartment() {
	return department;
}
public void setDepartment(Set department) {
	this.department = department;
}

}
