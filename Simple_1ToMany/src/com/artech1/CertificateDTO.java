package com.artech1;

public class CertificateDTO {
private int c_id;
private String name,courseName;
private int emp_id;
public int getC_id() {
	return c_id;
}
public void setC_id(int c_id) {
	this.c_id = c_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getCourseName() {
	return courseName;
}
public void setCourseName(String courseName) {
	this.courseName = courseName;
}
public int getEmp_id() {
	return emp_id;
}
public void setEmp_id(int emp_id) {
	this.emp_id = emp_id;
}
}
