package com.artech1;

import java.util.Scanner;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Customer_Transaction_Update {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		Transaction_Class tc = (Transaction_Class) s.get(Transaction_Class.class, 1);
        Set set = tc.getTrance();
        tc.setTx_date(2017);
        for (Object object : set) {
        	Customer_TX_Class ctx = (Customer_TX_Class) object;
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the name");
			String srt =sc.next();
			ctx.setC_name(srt);
			
		}
        s.update(tc);
        t.commit();
        s.close();
	}

}
