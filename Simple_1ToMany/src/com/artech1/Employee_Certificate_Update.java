package com.artech1;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Employee_Certificate_Update {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		EmployeeDTO ed = new EmployeeDTO();
	    ed.setEmp_id(1);
		ed.setName("Jitendraaaaaaa ");
        CertificateDTO cd = new CertificateDTO();
         cd.setC_id(1);
        cd.setName("Shyammmmmmm ");
        cd.setCourseName("java advance");
        CertificateDTO cd1 = new CertificateDTO();
       cd1.setC_id(2);
        cd1.setName("Rammmmmm ");
        cd1.setCourseName("java");
        CertificateDTO cd2 = new CertificateDTO();
        cd2.setC_id(3);
        cd2.setName("Sumittttttt");
        cd2.setCourseName("Android pp");
       
        Set set =new HashSet();
        set.add(cd);
        set.add(cd1);
        set.add(cd2);
        ed.setEmployee(set);
        Transaction t = s.beginTransaction();
        s.update(ed);
        t.commit();
       // System.out.println("Update is done");
	}

}
