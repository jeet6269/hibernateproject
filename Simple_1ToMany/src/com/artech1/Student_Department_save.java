package com.artech1;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class Student_Department_save {

	public static void main(String[] args) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		DepartmentDto dd = new DepartmentDto();
		dd.setD_name("Computer");
		//dd.setD_id(1001);
		dd.setD_head("Rahul Soni");
		StudentDto sd = new StudentDto();
		sd.setFname("raman");
		sd.setLname("mishra");
		//sd.setStud_id(201);
		sd.setAddress("vijay nagar");
		StudentDto sd2 = new StudentDto();
		sd2.setFname("Suman");
		sd2.setLname("tiwari");
		//sd2.setStud_id(202);
		sd2.setAddress("sceeam no 78");
		Set set = new HashSet();
		set.add(sd);
		set.add(sd2);
		dd.setDepartment(set);
		Transaction t = s.beginTransaction();
		s.save(dd);
		t.commit();
		s.close();
		
	}

}
