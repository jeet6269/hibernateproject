package com;
// Generated 8 Jul, 2017 3:21:38 PM by Hibernate Tools 5.2.3.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * LocationTable generated by hbm2java
 */
@Entity
@Table(name = "location_table", catalog = "taskdatabase")
public class LocationTable implements java.io.Serializable {

	private int LId;
	private EventTable eventTable;
	private String LName;
	private String city;

	public LocationTable() {
	}

	public LocationTable(int LId) {
		this.LId = LId;
	}

	public LocationTable(int LId, EventTable eventTable, String LName, String city) {
		this.LId = LId;
		this.eventTable = eventTable;
		this.LName = LName;
		this.city = city;
	}

	@Id

	@Column(name = "l_id", unique = true, nullable = false)
	public int getLId() {
		return this.LId;
	}

	public void setLId(int LId) {
		this.LId = LId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "e_location")
	public EventTable getEventTable() {
		return this.eventTable;
	}

	public void setEventTable(EventTable eventTable) {
		this.eventTable = eventTable;
	}

	@Column(name = "l_name")
	public String getLName() {
		return this.LName;
	}

	public void setLName(String LName) {
		this.LName = LName;
	}

	@Column(name = "city")
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
